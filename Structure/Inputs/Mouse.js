let Mouse = {
    btnPressed: [],
    position: {}
}

function getMousePos(e) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: e.clientX - rect.left,
      y: e.clientY - rect.top
    };
  }
  
document.addEventListener('pointermove', (e) => {
    Mouse.position = getMousePos(e)
})

//e.button -> 0: left | 1: wheel | 2: rigth
document.addEventListener('pointerdown', (e) => {
    Mouse.btnPressed[e.button] = true
})

document.addEventListener('pointerup', (e) => {
    Mouse.btnPressed[e.button] = false
})