class GameManager extends GameObject {

    //TODO Create for this game a Class or Interface Damageble to manager the life and your damage

    Awake(params){

        this.gameStartTime = Time.GetTimestamp()

        this.home = new Home(canvas.width/2, canvas.height/2, {w:75, h:75})

        this.tank = new Tank(canvas.width/2, canvas.height/2, {w: 50, h:50})

        this.enemies = []

        this.difficultAddTime = 10000
        this.nextDifficultAddTime = this.difficultAddTime

        this.enemyRespawn = 2000
        this.enemyType = 1

        this.CreateEnemy()
        this.currentLoopId = setInterval( () => {this.CreateEnemy()}, this.enemyRespawn)

    }

    CreateEnemy() {
        const rafflePosition = this.RaffleOutPosition()
        let enemy = this.RaffleAndCreateEnemy(this.enemyType, rafflePosition)
        enemy.SetTarget(this.home)
        this.enemies.push(enemy)
    }

    RaffleAndCreateEnemy(enemies, rafflePosition) {

        const enemyId = parseInt(Math.random() * enemies) 

        switch(enemyId) {
            case 0: 
                return (
                new Bomber(
                        rafflePosition.x , rafflePosition.y, 
                        {w: 20, h:20, speed: 2, tag: "Enemy"}
                    ))
            case 1: return (
                new Eater(
                    rafflePosition.x , rafflePosition.y, 
                    {w: 30, h:30, speed: 1.5, tag: "Enemy"}
                ))
            case 2:
                return (
                new Flyer(
                    rafflePosition.x , rafflePosition.y, 
                    {w: 20, h:20, speed: 3, tag: "Enemy"}
                ))
            default: null
        }

    }

    RaffleOutPosition() {

        const sideId = parseInt(Math.random() * 4) + 1

        switch(sideId) {
            case 1: //left
                return{x: 0, y: Math.random() * canvas.height}
            case 2: //top
                return{x: Math.random() * canvas.width, y: 0}
            case 3: //right
                return{x: canvas.width, y: Math.random() * canvas.height}
            case 4: //bottom
                return{x: Math.random() * canvas.width, y: canvas.height}
        }

        return {x: 0, y: 0}

    }

    Update() {

        if(Time.GetTimestamp() - this.gameStartTime > this.nextDifficultAddTime) {
            
            clearInterval(this.currentLoopId);
            
            this.nextDifficultAddTime += this.difficultAddTime
            this.enemyRespawn -= this.enemyRespawn <= 1200 ? 0 : 100 
            this.enemyType += this.enemyType >= 3 ? 0 : 1
            
            this.currentLoopId = setInterval( () => {this.CreateEnemy()}, this.enemyRespawn)
        
        }

    }

    Draw (ctx) {

        ctx.font = "15px Arial"
        ctx.textAlign = "center"
        ctx.fillText(
            "Time: " + parseInt((Time.GetTimestamp() - this.gameStartTime) / 1000), 
            canvas.width/2, 20
        )

    }

}