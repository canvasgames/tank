class Eater extends GameObject {

    Awake(params){
        this.w = params.w
        this.h = params.h
        this.speed = params.speed

        this.pivot = {
            x: this.w/2,
            y: this.h/2
        }      

        this.angle = 0

        this.maxLife = 50
        this.currentLife = this.maxLife

        this.power = 5
        this.bodyDamage = 25

        this.eatingTime = 1500
        this.currentEatingTime = 0

    }

    Update() {

        this.Movemment();

    }

    SetTarget(target){
        this.target = target
        this.SeekTarget()
    }

    SeekTarget() {
        this.angle = 
            Math.atan2(
                (this.target.y) - (this.y + this.pivot.y),
                (this.target.x) - (this.x + this.pivot.x),
            ) * 180 / Math.PI
    }

    Movemment(){

        if(!this.target) return
        if(Math.hypot(
            (this.target.x) - (this.x + this.pivot.x),  
            (this.target.y) - (this.y + this.pivot.y)
        ) <= this.target.w/2) {
            this.BiteTarget()
            return;
        }

        this.SeekTarget()

        var rad = this.angle * Math.PI / 180;

        this.x += Math.cos(rad) * this.speed * Frame.DeltaTime()
        this.y += Math.sin(rad) * this.speed * Frame.DeltaTime()
    }

    Damage(value) {
        this.currentLife -= value
        if(this.currentLife <= 0){
            this.Destroy()
        }
    }

    BiteTarget() {
        if(this.CanBite()){
            this.target.Damage(this.power)
            this.currentEatingTime = this.eatingTime + Time.GetTimestamp()
        }
    }

    CanBite(){
        return this.currentEatingTime < Time.GetTimestamp()
    }

    Draw (ctx) {

        ctx.fillStyle = "#550000"
        ctx.fillRect(this.x, this.y, this.w, this.h)
        ctx.stroke()
        ctx.restore()

        ctx.font = "10px Arial"
        ctx.textAlign = "center"
        ctx.fillText(this.currentLife, this.x + this.pivot.x, this.y + this.pivot.y)

    }

}