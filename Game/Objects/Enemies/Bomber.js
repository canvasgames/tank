class Bomber extends GameObject {

    Awake(params){
        this.w = params.w
        this.h = params.h
        this.speed = params.speed

        this.pivot = {
            x: this.w/2,
            y: this.h/2
        }      

        this.angle = 0

        this.maxLife = 30
        this.currentLife = this.maxLife

        this.power = 10
        this.bodyDamage = this.power

    }

    Update() {

        this.Movemment();

    }

    SetTarget(target){
        this.target = target
        this.SeekTarget()
    }

    SeekTarget() {
        this.angle = 
            Math.atan2(
                (this.target.y) - (this.y + this.pivot.y),
                (this.target.x) - (this.x + this.pivot.x),
            ) * 180 / Math.PI
    }

    Movemment(){

        if(!this.target) return
        if(Math.hypot(
            (this.target.x) - (this.x + this.pivot.x),  
            (this.target.y) - (this.y + this.pivot.y)
        ) <= this.target.w/2) this.AutoDestruction()

        this.SeekTarget()

        var rad = this.angle * Math.PI / 180;

        this.x += Math.cos(rad) * this.speed * Frame.DeltaTime()
        this.y += Math.sin(rad) * this.speed * Frame.DeltaTime()
    }

    Damage(value) {
        this.currentLife -= value
        if(this.currentLife <= 0){
            this.Destroy()
        }
    }

    AutoDestruction() {
        this.target.Damage(this.power)
        this.Destroy()
    }

    Draw (ctx) {

        ctx.fillStyle = "#FF00FF"
        ctx.fillRect(this.x, this.y, this.w, this.h)
        ctx.stroke()
        ctx.restore()

        ctx.font = "10px Arial"
        ctx.textAlign = "center"
        ctx.fillText(this.currentLife, this.x + this.pivot.x, this.y + this.pivot.y)

    }

}