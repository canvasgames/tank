class Home extends GameObject {

    Awake(params){
        this.w = params.w
        this.h = params.h

        this.pivot = {
            x: this.w/2,
            y: this.h/2
        }

        this.maxLife = 100
        this.currentLife = this.maxLife

    }

    Update() {

    }

    Damage(value) {
        this.currentLife -= value
        if(this.currentLife <= 0){
            alert("GameOver!!! The home was destroyed...")
            location.reload()
        }
    }

    Draw (ctx) {

        ctx.fillStyle = "#BBB"
        ctx.fillRect(this.x - this.pivot.x, this.y - this.pivot.y, this.w, this.h)
        ctx.stroke()
        ctx.restore()

        ctx.font = "20px Arial"
        ctx.textAlign = "center"
        ctx.fillText(this.currentLife, this.x, this.y);

    }

}