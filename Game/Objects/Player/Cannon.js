class Cannon extends GameObject {

    Awake(params) {
        this.w = params.w
        this.h = params.h

        this.freezeTime = 200
        this.nextFreezeTime = 0

        this.pivot = {
            x: this.w/2,
            y: this.h
        }

        this.myAngle = 0
        this.startAngle = 90

    }

    Update() {

        this.myAngle = 
            Math.atan2(
                this.y + this.pivot.y - Mouse.position.y, 
                this.x + this.pivot.x - Mouse.position.x
            ) * 180 / Math.PI
        this.myAngle -= this.startAngle //adjust angle

        if(Mouse.btnPressed[0] && this.CanShoot()){

            var bulletW = 10
            var bulletH = bulletW

            var angle = (this.myAngle - this.startAngle) * -1 //adjust angle
            var rad = (angle) * Math.PI / 180;

            var startX = 
                (Math.cos(rad) * this.h) + this.x + this.pivot.x - (bulletW / 2)
            
            var startY = 
                -(Math.sin(rad) * this.h) + this.y + this.pivot.y - (bulletH / 2)

            new Bullet(startX, startY, 
                        {
                            w:bulletW, 
                            h:bulletH, 
                            speed: 4, 
                            angle: this.myAngle - this.startAngle
                        })
            this.nextFreezeTime = Time.GetTimestamp() + this.freezeTime
        }

    }

    CanShoot(){
        return this.nextFreezeTime < Time.GetTimestamp()
    }

    Draw (ctx) {

        var rad = this.myAngle * Math.PI / 180;
        ctx.translate(this.x + this.pivot.x, this.y + this.pivot.y);
        
        ctx.rotate(rad)
        ctx.fillStyle = "#007700"
        ctx.fillRect(-this.pivot.x,-this.pivot.y,this.w,this.h)
        ctx.stroke()

        ctx.restore()

        if(false){

            ctx.beginPath();
            ctx.fillStyle='red';
            ctx.fillRect(this.x + this.pivot.x -1, this.y + this.pivot.y -1, 3, 3);
            ctx.stroke();

            ctx.beginPath();
            ctx.moveTo(this.x + this.pivot.x, this.y + this.pivot.y);
            ctx.lineTo(Mouse.position.x, Mouse.position.y);
            ctx.stroke();

            ctx.restore()
        }

    }

}