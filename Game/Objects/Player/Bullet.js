class Bullet extends GameObject {

    Awake(params){
        this.w = params.w
        this.h = params.h

        this.pivot = {
            x: this.w/2,
            y: this.h/2
        }

        this.initAngle = params.angle

        this.speed = params.speed

        var lifeTime = 1000
        this.destroyTime = Time.GetTimestamp() + lifeTime

        this.power = 10

    }

    Update() {

        this.Movemment()

        this.CheckCollision()

        if(this.destroyTime < Time.GetTimestamp()){
            this.Destroy()
        }

    }

    Movemment() {
        var rad = this.initAngle * Math.PI / 180;

        //TODO Remove addX and addY
        var addX = Math.cos(rad) * this.speed * Frame.DeltaTime()
        var addY = Math.sin(rad) * this.speed * Frame.DeltaTime()

        // console.log("Cos: ", addX, "Sin: ", addY)

        this.x += addX
        this.y += addY
    }

    CheckCollision() {
        GameObjectManager.TaggedGameObjects["Enemy"].forEach(go => {
            if(go){
                if(
                    this.x + this.w >= go.x && this.x <= go.x + go.w &&
                    this.y + this.h >= go.y && this.y <= go.y + go.h
                ) {
                    go.Damage(this.power)
                    this.Destroy()
                }
            }
        })
    }

    Draw (ctx) {

        ctx.fillStyle = "#555"
        ctx.fillRect(this.x, this.y, this.w, this.h)
        ctx.stroke()

    }

}