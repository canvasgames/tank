class Tank extends GameObject {

    Awake(params) {
        this.w = params.w
        this.h = params.h

        this.pivot = {
            x: this.w/2,
            y: this.h/2
        }

        this.maxSpeed = 1 

        //TODO: w and h Cannon, be a Get in Cannon Class
        this.wCannon = 10
        this.hCannon = 40
        var xCannon = this.x + this.w/2 - this.wCannon/2
        var yCannon = this.y - this.h/2
        this.cannon = new Cannon(xCannon, yCannon, {w: this.wCannon, h: this.hCannon})

        this.maxLife = 100
        this.currentLife = this.maxLife

    }

    Update(){

        if(Keyboard.keyPressed["KeyA"]){ this.MoveTank(-this.maxSpeed * Frame.DeltaTime(), 0) }
        if(Keyboard.keyPressed["KeyD"]){ this.MoveTank(this.maxSpeed * Frame.DeltaTime(), 0) }
        if(Keyboard.keyPressed["KeyW"]){ this.MoveTank(0, -this.maxSpeed * Frame.DeltaTime()) }
        if(Keyboard.keyPressed["KeyS"]){ this.MoveTank(0, this.maxSpeed * Frame.DeltaTime()) }

        this.DetectCollisionWithEnemy()
    
    }

    DetectCollisionWithEnemy() {
        GameObjectManager.TaggedGameObjects["Enemy"].forEach(go => {
            if(go){
                if(
                    this.x + this.w >= go.x && this.x <= go.x + go.w &&
                    this.y + this.h >= go.y && this.y <= go.y + go.h
                ) {
                    this.Damage(go.bodyDamage)
                    go.Destroy()
                }
            }
        })
    }

    MoveTank(newX, newY){

        this.x += newX;
        this.y += newY;

        //TODO: w and h Cannon, uses Get of your class
        this.cannon.x = this.x + this.w/2 - this.wCannon/2
        this.cannon.y = this.y - this.h/2

    }

    Damage(value) {
        this.currentLife -= value
        if(this.currentLife <= 0){
            alert("GameOver!!! The tank was destroyed...")
            location.reload()
        }
    }

    Draw(ctx) {

        ctx.fillStyle = "#008800"
        ctx.fillRect(this.x, this.y, this.w, this.h)
        ctx.stroke()
        ctx.restore()

        ctx.font = "10px Arial"
        ctx.textAlign = "center"
        ctx.fillText(this.currentLife, this.x + this.pivot.x, this.y + this.pivot.y)
    
    }

}